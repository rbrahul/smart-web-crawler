const express = require('express');
const router = express.Router();
const { startCrawling, downloadResult } = require('./../controllers/crawlers');

router.get('/', startCrawling);
router.get('/download-file', downloadResult);

module.exports = router;
