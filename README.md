# Smart Web Crawler
### This is an automated Web Crawler or BOT that can  grab the content of thousands of web pages within minutes.

## How to start:
Clone this git repository and run following commands after entering into the directory:

 ```
 npm install
 ```
 ```
 npm start
 ```

## Requirements:
This implementation uses several ES6/ES7 Syntax on server side without polyfill or babelify. That's why you need to install at least node v.8.6.0 or later.

## Features:
- Grabing thousands of web pages within minutes
- No database needed
- Realtime statistics
- Downloadable result in plain JSON format.
- Simple and declarative UI/UX
- Classified Resources

## Technology Used:
- Node.js, Express
- Socket.io
- JQuery
- Bootstrap 4+

## Screenshot
![Screenshot of dashbord](https://bitbucket.org/rbrahul/smart-web-crawler/raw/44779941493c5282d4c5f713ddd7a3441ceb54e2/public/images/screenshot-1.png "Dashbord")

## Note:
 Enjoy the speed but don't hurt good pleople :)