const fs = require('fs');
const path = require('path');
const { crawlURL } = require('./../services/crawler');
const DOMparser = require('./../services/DOMparser');

var EVENTS = {
    URL_CRAWLED: 'URL_CRAWLED',
    CRAWLING_SUCCESS: 'CRAWLING_SUCCESS',
};

let crawledResult = {
    internal: [],
    external: []
};


let baseURL = '';
let alreadyCrawledURLs = [];
let crawlableURLsQueue = [];

const initCrawler = () => {
    baseURL = '';
    stopCrawling = false;
    alreadyCrawledURLs = [];
    crawlableURLsQueue = [];
    crawledResult = {
        internal: [],
        external: []
    };
    delete global.STOP_CRAWLING;
};

const setURL = url => {
    const URLInstance = new URL(url);
    baseURL = URLInstance.origin;
    crawlableURLsQueue = [url];
};


/* 
 ** push current URL to alreadyCrawledURLs
 ** delete current URL from crawlableURLsQueue
    @return void
 */
const modifyCrawlableURL = () => {
    alreadyCrawledURLs.push(crawlableURLsQueue[0]);
    crawlableURLsQueue.shift();
}

const getUnique = (existingList, newList) => {
    return newList.filter(item => {
        return !existingList.includes(item);
    });
}

const rtrim = (text, charcter) => {
    if (!text) {
        return;
    }

    while (text.endsWith(charcter)) {
        text = text.substring(0, text.lastIndexOf(charcter));
    }
    return text;
};

/*
** Add New URLs to the crawlable queue
*/
const addURLsToCrawlableQueue = (internalURLs) => {
    internalURLs.forEach(item => {
        let url = rtrim(item.trim(), '/');
        if (
            !url ||
            url.includes('#') ||
            (!url.startsWith('/') && alreadyCrawledURLs.includes(url))
        ) {
            return;
        }
        if (
            !/^((http[s]?|ftp):\/\/)/.test(item) &&
            !item.startsWith('#')
        ) {
            url = baseURL+'/' + url;
        }

        if (!alreadyCrawledURLs.includes(url) && !crawlableURLsQueue.includes(url) && url.startsWith('http')) {
            crawlableURLsQueue.push(url);
        }
    });
}

/* 
 ** Crawl urls recursively
    @return void
 */
const processCrawling = async (io) => {
    if (crawlableURLsQueue.length == 0 || global.STOP_CRAWLING) {
        console.log(`CRAWLING SUCCESSFULL FOR ${baseURL}`);
        try {
            fs.writeFile('./public/reports/result.json', JSON.stringify(crawledResult, null, 2), error => {
                if (error) {
                    console.log('FAILED TO WRITE THE RESULT TO DISK', error);
                }
            });
            io.emit(EVENTS.CRAWLING_SUCCESS);
        } catch (error) {
        }
        return;
    }

    let currentURL = crawlableURLsQueue[0];
    const pageContent = await crawlURL(currentURL);
    if (pageContent) {
        const parsedDOM = new DOMparser(pageContent, baseURL);
        const { internalURLs, externalURLs } = parsedDOM.getCategorizedURLs();
        const uniqeuExternalURLS = getUnique(crawledResult.external, externalURLs);
        crawledResult.external = [...crawledResult.external, ...uniqeuExternalURLS];
        const crawledData = {
            url: currentURL,
            body: '',//pageContent,
            title: parsedDOM.getTitle(),
            externalURLs: uniqeuExternalURLS,
            time: new Date(),
        }
        crawledResult.internal.push(crawledData);
        io.emit(EVENTS.URL_CRAWLED, crawledData);
        modifyCrawlableURL();
        addURLsToCrawlableQueue(internalURLs);
    } else {
        modifyCrawlableURL();
        crawledResult.internal.push({
            url: currentURL,
            body: '',
            title: '',
            time: new Date()
        });
    }

    // start with new URL in recurssive mode
    currentURL = crawlableURLsQueue[0];
    processCrawling(io);
};

const startCrawling = (req, res) => {
    try {
        initCrawler();
        let crawlableURL = req.query.url.trim();
        setURL(rtrim(crawlableURL, '/'));
        processCrawling(req.app.io);
    } catch (error) {
        console.log('ERROR FOUND WHILE CRAWLING', error);
    }

    return res.json({
        message: 'URL is being fetch. Grab a Coffee :)'
    });
};

const downloadResult = (req, res) => {
    try {
        const filePath = path.resolve(path.join(__dirname, './../public/reports/result.json'));
        return res.download(filePath);
    } catch (error) {
        return res.json({
            message: 'Oops, Something went wrong :( !'
        });
    }
}

module.exports = {
    startCrawling,
    downloadResult
};