var socket = io();

var dataStore = {
    internalsURLs: [],
    externalURLs: []
};
var internalURLDatatable;
var externalURLDatatable;
var allURLDatatable;
var startTime;
var timeInterval;

var EVENTS = {
    URL_CRAWLED: 'URL_CRAWLED',
    STOP_CRAWLING: 'STOP_CRAWLING',
    CRAWLING_SUCCESS: 'CRAWLING_SUCCESS',
};

socket.on('connection', function (info) {
    console.log('Socket Connected on Client..!');
});

socket.on(EVENTS.URL_CRAWLED, function (msg) {
    dataStore.internalsURLs.push(msg);
    msg.id = dataStore.internalsURLs.length;
    msg.url = '<a href="' + msg.url + '" target="_blank">' + msg.url + '</a>';
    internalURLDatatable.row.add(msg).draw(false);
    var counter = dataStore.externalURLs.length;
    externalURLDatatable.rows.add(msg.externalURLs.map(url => {
        return {
            id: ++counter,
            url: '<a href="' + url + '" target="_blank">' + url + '</a>'
        };
    })).draw(false);
    dataStore.externalURLs = dataStore.externalURLs.concat(msg.externalURLs);
    updateCounter();
    updateAllURLDataTable();
});

socket.on(EVENTS.CRAWLING_SUCCESS, function (msg) {
    $('.preloader').hide();
    showFileBtn();
    clearInterval(timeInterval);
});

function updateCounter() {
    $('#internal-urls-counter').text(dataStore.internalsURLs.length);
    $('#external-urls-counter').text(dataStore.externalURLs.length);
    $('#total-urls-counter').text(dataStore.externalURLs.length + dataStore.internalsURLs.length);
}

function initalize() {
    dataStore = {
        internalsURLs: [],
        externalURLs: []
    };
    startTime = new Date();
    updateCounter();
    internalURLDatatable.clear();
    externalURLDatatable.clear();
    allURLDatatable.clear();
    $('.preloader').hide();
    showStopBtn();
}

function updateAllURLDataTable() {
    var urls = [];
    var counter = 0;
    dataStore.internalsURLs.forEach(item => {
        var urlObj = {
            id: ++counter,
            title: item.title,
            url: item.url,
            time: item.time,
            type: '<span class="btn btn-xs btn-outline-success">Internal</span>',
        };
        urls.push(urlObj);

        if (item.externalURLs && item.externalURLs.length > 0) {
            var externalURLs = item.externalURLs.map(exurl => {
                return {
                    id: ++counter,
                    title: '-',
                    url: '<a href="' + exurl + '" target="_blank">' + exurl + '</a>',
                    time: '-',
                    type: '<span class="btn btn-xs btn-outline-primary">External</span>',
                }
            });
            urls = urls.concat(externalURLs);
        }
    });
    allURLDatatable.clear().rows.add(urls).draw(false);
    return urls;
}

function showFileBtn() {
    $('.stop-card').hide();
    $('.file-card').show();
}

function showStopBtn() {
    $('.file-card').hide();
    $('.stop-card').show();
    $('.stop-btn').show();
}

function manageTimer() {
    timeInterval = setInterval(() => {
        var time = timeDiff(startTime, new Date());

        var timeStr = (time.sec < 10 ? '0' + time.sec : time.sec) + ' sec';
        if (time.min) {
            timeStr = (time.min < 10 ? '0' + time.min : time.min) + ' min ' + timeStr;
        }
        if (time.hour) {
            ttimeStr = (time.min < 10 ? '0' + time.hour : time.hour) + ' hour ' + timeStr;
        }

        $('.timer').text(timeStr);
    }, 1000);
}

function timeDiff(start, end) {
    var msec = end.getTime() - start.getTime();
    var hh = Math.floor(msec / 1000 / 60 / 60);
    msec -= hh * 1000 * 60 * 60;
    var mm = Math.floor(msec / 1000 / 60);
    msec -= mm * 1000 * 60;
    var ss = Math.floor(msec / 1000);
    msec -= ss * 1000;
    return {
        hour: hh,
        min: mm,
        sec: ss,
    };
}

$(document).ready(function () {
    //INIT DATA TABLE 
    internalURLDatatable = $('#dataTable-internal-url').DataTable({
        data: () => {
            return dataStore.internalsURLs.map(item, ind => {
                return {
                    id: ind + 1,
                    url: item.url,
                    title: item.title,
                    time: item.time.toString()
                };
            })
        },
        'order': [[0, 'desc']],
        "columns": [
            { "data": "id" },
            { "data": "url" },
            { "data": "title" },
            { "data": "time" },
        ]
    });

    allURLDatatable = $('#dataTable-all-url').DataTable({
        data: () => {
            return dataStore.internalsURLs.map(item, ind => {
                return {
                    id: ind + 1,
                    url: item.url,
                    title: item.title,
                    time: item.time.toString()
                };
            })
        },
        'order': [[0, 'desc']],
        "columns": [
            { "data": "id" },
            { "data": "url" },
            { "data": "title" },
            { "data": "time" },
            { "data": "type" },
        ]
    });

    externalURLDatatable = $('#dataTable-external-url').DataTable({
        data: () => {
            return dataStore.externalURLs.map(item, ind => {
                return {
                    id: ind + 1,
                    url: item.url,

                };
            })
        },
        'order': [[0, 'desc']],
        "columns": [
            { "data": "id" },
            { "data": "url" },
        ]
    });



    $('.crawl-form').on('submit', function (e) {
        e.preventDefault();
        var url = $.trim($('.url-field').val());
        var pat = /^(?:http(s)?:\/\/)[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm;
        if (!pat.test(url)) {
            alert('Enter valid URL');
            return false;
        } else {
            initalize();
            manageTimer();
            $('.preloader').show();
            $.ajax({
                url: '/crawl',
                method: 'GET',
                data: {
                    url: url
                },
                onSuccess: function (data) {
                    console.log(data);
                },
                onError: function (error) {
                    console.log('Something went worng', error);
                }
            });
        }
        return false;
    });

    $('.data-details-btn').click(function (e) {
        e.preventDefault();
        var id = $(this).data('tableid');
        $('.rb-table-holder').hide();
        $(id).show();
    });

    $('.stop-btn').click(function (e) {
        e.preventDefault();
        socket.emit(EVENTS.STOP_CRAWLING);
    })
});
