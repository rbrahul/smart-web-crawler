const { URL } = require('url');
const cheerio = require('cheerio');

module.exports = class DOMParser {
    constructor(pageConent, url) {
        this.$ =  cheerio.load(pageConent);
        this.URLs = [];
        this.parsedURL = new URL(url);
        this._getAllURLs();
    }

    _getAllURLs() {
        this.$('a').each((index, node) => {
            const URL = this.$(node).attr('href');
            if (URL && URL.trim()) {
                this.URLs.push(URL);
            }
        });
    }

    _isURL(url) {
        try{
            new URL(url)
            return true;
        } catch(err) {
            return false;
        }
    }

    hasValidProtocal(url) {
        var pat= /^((http[s]?|ftp):\/\/)/i
        return pat.test(url);
    }

    isInternalURL(url) {
        return (
        !this.hasValidProtocal(url) ||
        url.startsWith('/') ||
        url.startsWith('./') ||
        url.startsWith('#') ||
        (this._isURL(url) && (new URL(url).host).includes(this.parsedURL.hostname.replace(/^www\./gi, '')))
        );
    }

    getTitle() {
        if (this.$('title').length) {
            return this.$('title').text();
        };
        return '';
    }

    getCategorizedURLs() {
        const urls = {
            internalURLs: [],
            externalURLs: []
        };

        if (this.URLs.length == 0) {
            return urls;
        }

        this.URLs.forEach((item) => {
            if(this.isInternalURL(item)) {
                urls.internalURLs.push(item);
            } else {
                urls.externalURLs.push(item);
            }
        });

        return urls;
    }

}
