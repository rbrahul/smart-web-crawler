
const fetch = require('node-fetch');

const crawlURL = async url => {
    try {
        const page = await fetch(url);
        if (!page.ok) {
            throw new Error('Failed to Load Content');
        }
        return await page.text();
    } catch (error) {
        return null;
    }
};

module.exports = {
    crawlURL,
};
